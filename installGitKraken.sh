#!bin/bash

echo "INSTALLING GITKRAKEN"

#HAVE TO BE IN SUDO FOR THESE NEXT STEPS

cd /opt
cd /opt

echo "DOWNLOADING MOST RECENT GITKRAKEN"
sudo wget https://release.gitkraken.com/linux/gitkraken-amd64.tar.gz
sudo tar -xvzf gitkraken-amd64.tar.gz
sudo rm gitkraken-amd64.tar.gz

sudo echo "export PATH=\$PATH:/opt/gitkraken" >> ~/.bashrc
sudo source ~/.bashrc

#GETTING ICON PICTURE
sudo wget http://img.informer.com/icons_mac/png/128/422/422255.png
sudo mv 422255.png ./gitkraken/icon.png
 
#CREATING FILE
sudo touch /usr/share/applications/gitkraken.desktop

#CREATING DESKTOP FILE 
sudo echo "
[Desktop Entry]
Name=GitKraken
Comment=Git Flow
Exec=/opt/gitkraken/gitkraken
Icon=/opt/gitkraken/icon.png
Terminal=false
Type=Application
Encoding=UTF-8
Categories=Utility;Development;" | sudo tee -a /usr/share/applications/gitkraken.desktop >/dev/null

########################################################
## IMPORTANT OTHERWISE IT WONT WORK ON FEDORA SYSTEMS ##
########################################################

cd /opt/gitkraken
ln -s /usr/lib64/libcurl.so.4 libcurl-gnutls.so.4

